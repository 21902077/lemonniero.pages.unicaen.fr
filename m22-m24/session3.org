#+title: M22, Session #3
#+language: fr
#+select_tags: export
#+exclude_tags: noexport
#+html_link_home: http://lemonniero.pages.unicaen.fr/
#+html_link_up: http://lemonniero.pages.unicaen.fr/m22-m24.html

#+index: environnement
#+index: processus
#+index: paquetages

* Enseignement à distance

Pour réaliser les TP à distance, vous pouvez utiliser la machine
virtuelle Ravada (https:/vdi.unicaen.fr) nommée LPASRSI-recap (compte
=root=, mot de passe 'Pr1v1legie!') accessible en SSH selon les
modalités décrites par le schéma ci-dessous).  *Changez le mot de passe
dès que possible*.

#+caption: Architecture réseau Ravada
[[./images/Réseau-Ravada.png]]

* Système de fichiers

Les fichiers sont désignés par leur nom complet (depuis la racine) ou
relatif (depuis le répertoire courant).  Le nom d'un fichier peut
contenir n'importe quel caractère sauf =/= qui utilisé pour identifier
le chemin d'accès à un fichier.  Il est toutefois préférable d'éviter
les caractères spéciaux : =*=, =()=, =~=, =[]=, =|=, =&=, =;=, =<>=,
/espace/, =$=, =\=, ', ", =?= et =!=.  Il faut également éviter
d'utiliser =-= comme premier caractère.

Chaque fichier correspond à un /inode/ ([[https://fr.wikipedia.org/wiki/N%C5%93ud_d%27index][nœud d'index]] ou i-nœud)
unique, propre au système de fichiers sur lequel le fichier est stocké
(/cf/ =ls -i=).  Les inodes sont créés au moment du formatage du
système de fichiers du périphérique.  Un inode est une structure de
données contenant les informations relatives au fichier (métadonnées).
POSIX impose que les inodes des fichiers présentent les informations
suivantes :
  - la taille du fichier en octets
  - le type de fichier (régulier : =-=, répertoire : =d=, lien : =l=,
    périphériques de type caractère : =c=, pile FIFO : =p=,
    périphériques de type bloc : =b=)
  - le nombre de lien vers l'inode
  - le numéro d'inode,
  - l'identifiant du périphérique,
  - l'identifiant de l'utilisateur propriétaire, l'=uid=
  - l'identifiant du groupe propriétaire, le =gid=
  - les permissions d'accès en lecture (=r=), écriture (=w=) et
    exécution (=x=) pour le propriétaire (=u=), le groupe propriétaire
    (=g=) et les autres (/others/) utilisateurs du système (=o=).
  - 3 bits supplémentaires
  - les dates : de dernière modification /mtime/ du fichier (=ls -l=),
    de dernière modification /ctime/ de l'inode (=ls -lc=) et du
    dernier accès /atime/ (=ls -lu=)
  - l'adresse des blocs disques contenant le fichier

Selon le système de fichiers, les inodes peuvent « contenir » d'autres
informations.  La commande =stat= (appel système du même nom : =man 1
stat= et =man 2 stat=) affiche un ensemble d'informations concernant le
fichier dont le nom doit être passé en argument.

* TD #2 : Inœuds

 1. Créer un répertoire /test/.  Trouver et lire la définition des
    options longues =all=, =directory= et =inode= dans la page de manuel de
    la commande =ls.= Essayer différentes combinaisons de ces options.
    Identifier le numéro d'inode du répertoire =test=.  *Quels sont les
    deux liens* (noms de fichiers) *vers l'inode du répertoire =test= ?*
 2. Créer un sous-répertoire /sub/ dans /test/.  Afficher le nombre de
    liens vers l'inode du répertoire /test/ en utilisant la commande
    =stat=.  *Quels sont les noms de ces liens ?*
 3. Dans le répertoire /test/, créer un fichier /foo/ avec la commande
    =echo blabla > test/foo= (=>= est le redirecteur de sortie).
    Identifier le numéro d'inode.  Créer un lien « dur » nommé /bar/
    du fichier /foo/ (=ln=).  *Quel est le numéro d'inode de* /bar/
    *?*
 4. Créer un lien symbolique nommé /baz/ du fichier /foo/ (=ln=).
    *Combien l'inode de* /foo/ *présente-t-il de liens maintenant ?*
 5. Déplacer le fichier /foo/ dans le répertoire parent.  Afficher le
    contenu de /bar/ et /baz/ (=cat=).  *Justifier.*
 6. Supprimer le fichier /foo/.  Afficher le contenu de /bar/ et
    /baz/.  *Justifier.*
* Les autorisations Linux

** Utilisateur, groupe, /others/

Sur un système Linux, tout utilisateur (réel ou processus) est
identifié par un numéro unique, appelé UID (/User IDentifier/).  Il
est également associé à un groupe principal, identifié par un numéro
unique nommé GID, et éventuellement à d'autres groupes.  L'utilisateur
ayant l'UID 0 est « spécial » car le système ne vérifie jamais ses
droits d'accès.

Chaque fichier :
  - est la propriété d'un utilisateur et d'un groupe,
  - et présente des permissions en lecture (=r=), écriture (=w=) et
    exécution (=x=).

** Cas des répertoires

Un répertoire est un fichier, donc associé à un /inode/, et le contenu —
en terme de données associées au répertoire — est une liste de noms de
fichiers stockés *et le numéro d'/inode/ correspondant*.  Le droit en
lecture sur le répertoire permet de lire le nom des fichiers, celui en
exécution de lire le numéro d'/inode/ correspondant.

** /Discretionary access control/

Ces permissions sont le fondement de la sécurité du système.  Elles
sont représentées par une suite de 9 bits =rwxrwxrwx= représentant les
droits droits énoncés ci-dessus pour l'utilisateur propriétaire, le
groupe propriétaire et le reste de utilisateurs du système.

Chaque groupe de 3 bits =rwx= peut aussi être représenté par sa valeur
octale entre 0 et 7.

| =r= (4) | =w= (2) | =x= (1) | *octal* |
|-------+-------+-------+-------|
|       |       |       |     0 |
|       |       | X     |     1 |
|       | X     |       |     2 |
|       | X     | X     |     3 |
| X     |       |       |     4 |
| X     |       | X     |     5 |
| X     | X     |       |     6 |
| X     | X     | X     |     7 |
#+TBLFM: $3=x= (1)

Ainsi la chaĩne =640= signifie :
  - accès en lecture et écriture pour le propriétaire,
  - accès en lecture pour le groupe propriétaire,
  - aucun accès pour les autres utilisateurs.

** Changer les permissions

Seul le propriétaire du fichier est autorisé à modifier les
permissions associées, avec la commande =chmod=.  Ce contrôle d'accès
est dit discrétionnaire (DAC : /discretionary access control/) car
laissé au choix de l'utilisateur.

Le principal argument de la commande =chmod= est l'ensemble de
permissions à affecter au fichier.  Cet ensemble peut être spécifié
avec la notation octale complète (/ex/ : =chmod 755 …=) ou en
utilisant une notation relative (/ex/ : =chmod u+w,g-w,o-rx …=) dans
laquelle :
  - =u= représente l'utilisateur propriétaire, =g= le groupe
    propriétaire et =o= le reste des utilisateurs,
  - =-= signifie la suppression du droit, *=* son maintien et =+=
    l'ajout,
  - =r=, =w= et =x= sont les droits eux-même.

Personne n'est autorisé à changer l'utilisateur ou le groupe
propriétaire d'un fichier.  Cependant les commandes =chown= et =chgrp=
existent !

** Bits supplémentaires

Trois bits supplémentaires, =set UID=, =set GID= et le =sticky= bit
permettent la mise en place de comportements supplémentaires.
  1. Le bit =suid= concerne les fichiers exécutables.  Lorsqu'il est
     actif,l'exécution s'effectue avec l'identité de l'utilisateur
     propriétaire et non celle de l'utilisateur l'ayant lancé.
  2. Le bit =sgid= présente des propriétés analogues au niveau groupe
     en ce qui concerne les fichiers exécutables.  Lorsqu'il est
     activé sur un répertoire, tout fichier créé dans le répertoire
     sera propriété du même groupe que celui du répertoire plutôt que
     propriété du groupe principal de l'utilisateur ayant créé le
     fichier.
  3. Le /sticky/ bit appliqué à un exécutable force sa persistance en
     mémoire après exécution.  Appliqué à un répertoire (/cf/ =/tmp=),
     il interdit la suppression des fichiers dans ce répertoire aux
     utilisateurs autres que le propriétaire du fichier (ce qui est
     possible en l'absence du /sticky/ bit, avec une autorisation =w=
     sur le répertoire).

La notation octale existe, ex : =chmod 1755 ./ip.sh=

* TD #3 : Droits POSIX

*Ne pas faire ce TD en tant que root*

 1. Créer un répertoire /test/, y créer un fichier /temp/ avec la
    commande =touch=.
 2. Vérifier les droits sur le répertoire et son contenu.  *Quelles
    commandes avez-vous utilisées ?*
 3. Supprimer les droits de lecture sur le répertoire (=chmod=).
    Lister son contenu.  *Constater puis justifier.*
 4. Rétablir les droits initiaux.  Supprimer les droits en écriture
    sur le fichier /temp/.  Supprimer ce fichier.  *Constater puis
    justifier.*

Pour aborder les questions suivantes, il est nécessaire de comprendre
quelles sont les données associées à un répertoire (/indice/ : elles
utilisent le concept d'/inode)/.

 5. Recréer le fichier /temp/.  Supprimer les droits en écriture sur
    le répertoire /test/.  Supprimer le fichier /temp/.  *Constater
    puis justifier.*
 6. Rétablir les droits initiaux.  Supprimer les droits d'exécution
    sur le répertoire /test/.  Lister son contenu.  *Constater puis
    justifier.*

* Contenus additionnels
** Arborescence des fichiers

L'arborescence des fichiers d'un système GNU/Linux est généralement
constituée des éléments suivants (liste non exhaustive).  Certains
sont « normalisés » par le [[https://fr.wikipedia.org/wiki/FHS][FHS]] (/Filesystem Hierarchy Standard/) du
[[https://fr.wikipedia.org/wiki/Linux_Standard_Base][LSB]] (/Linux Standard Base/), d'autres pas encore :
  - =/= :: la racine
  - =/boot= :: tout ce qui concerne le démarrage (/boot/) du système :
       le noyau, sa configuration, le disque mémoire d'initialisation
       pouvant contenir certains pilotes.
  - =/dev= :: fichiers spéciaux parmi lesquels ceux qui permettent
       d'accéder aux périphériques (/devices/).
  - =/etc= :: ensemble des fichiers de configuration du système
  - =/lib= :: bibliothèques (/libraries/ en anglais) communes
       utilisées par les logiciels exécutables.
  - =/tmp= et =/run= :: répertoires /temporaires/ requis par certains
       programmes.  =/tmp= occupe de l'espace disque alors que =/run=
       plus récent occupe une partie de la mémoire vive.
  - =/media= et =/mnt= :: répertoires dédiés au /montage/ des système
       de fichiers supplémentaires, =/media= pour les périphériques
       amovibles (/médias/), =/mnt= pour les autres.
  - =/bin=, =/sbin= :: programmes exécutables (/binaires/)
       indispensables au fonctionnement du système, =/sbin= contenant
       les programmes privilégiés.
  - =/usr= :: sous-arborescence contenant les programmes destinés aux
       /utilisateurs/ (au sens large) du système.
  - =/var= :: contient tout ce qui /varie/ au cours du cycle de vie du
       système.
  - =/proc= et =/sys= :: points de montage de pseudo systèmes de
       fichiers contenant toutes les informations relatives aux
       /processus/ en cours d'exécution sur le /système/
  - =/selinux= :: répertoire dédié au dispositif de sécurisation
       amélioré (/security enhanced/).
  - =/root= :: le répertoire de l'utilisateur privilégié (/root/).
  - =/home= :: les répertoires personnels, /maisons/, des utilisateurs
       non privilégiés.

/Cf/ =man hier=

* Obtenir les adresses IP des VM                                   :noexport:

#+begin_src  sh :dir /ssh:root@vdi.unicaen.fr:
echo 'Nom Prénom IP Port'
for id in $(virsh list | grep recap | tr -s ' ' ' ' \
            | cut -d' ' -f3 | sed s/LPASRSI_recap-// \
            | grep -v lemonniero | cut -d- -f1)
do
  mac=$(virsh dumpxml LPASRSI_recap-$id \
        | grep 'mac address' | cut -d"'" -f2)
  name=$(ldapsearch -xLLL -D "uid=authldap,ou=system,dc=unicaen,dc=fr" \
         -w "XXXXXXXX" -b "dc=unicaen,dc=fr" -h ldap "supannaliaslogin=$id" cn \
         | grep ^cn | cut -d: -f2)
  ip=$(arp | grep $mac | cut -d' ' -f1)
  last=$(echo $ip | cut -d. -f4)
  echo "$name $ip $last"
done
#+end_src

#+RESULTS:
| Nom         | Prénom    |  Etupass |              IP |   Port |
|-------------+-----------+----------+-----------------+--------|
| Aubert      | Corentin  | 22010561 | 192.168.144.123 |    123 |
| Fremin      | Quentin   | 21703114 | 192.168.144.208 |    208 |
| Martinelli  | Arthur    | 21601810 |                 |        |
| Osmont      | Valentine | 22006928 | 192.168.144.148 |    148 |
| Rivallant   | Stanislas | 22010562 | 192.168.144.165 |    165 |
| Robin       | Noemie    | 22005477 |   192.168.144.8 |      8 |
| Vanhecke    | Romain    | 22008811 | 192.168.144.151 |    151 |
|-------------+-----------+----------+-----------------+--------|
| Boutard     | Alexandre | 22011838 |  192.168.144.11 |     11 |
| Colette     | Morgane   | 22012701 |  192.168.144.87 |     87 |
| Cornet      | Killian   | 22010303 |  192.168.144.95 |     95 |
| Dermouchere | Jules     | 21701750 | 192.168.144.139 |    139 |
| Donatien    | Thomas    | 22011620 |          Absent | Absent |
| Foulon      | Antoine   | 21701362 |  192.168.144.41 |     41 |
| Jaffal      | Radouan   | 21608968 | 192.168.144.240 |    240 |
| Paris       | Nicolas   | 21803157 |  192.168.144.34 |     34 |
| Pitois      | Corentin  | 21700276 |  192.168.144.43 |     43 |
| Rihouey     | Andrea    | 21710154 | 192.168.144.243 |    243 |
| Thomas      | Cedric    | 21811646 |  192.168.144.78 |     78 |

