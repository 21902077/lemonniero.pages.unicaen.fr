;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Rasmus

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.

;;; Code:

(require 'package)
(package-initialize)
;; (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
;; (package-install 'org-plus-contrib)
(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

;; setting to nil, avoids "Author: x" at the bottom
(setq user-full-name "Olivier Le Monnier")
(setq user-mail-address "olm@unicaen.fr")

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc t)

(setq org-export-default-language "fr"
      org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%A %d %B %Y"
      org-html-checkbox-type 'unicode
      org-html-html5-fancy t
      org-html-validation-link t
      org-html-doctype "html5"
      org-src-fontify-natively t
      org-html-coding-system 'utf-8-unix)

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq org-publish-project-alist
      (list
       (list "site-org"
             :language 'fr'
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
             :with-author t
             :with-clocks nil
             :with-creator t
             :with-date nil
             :with-email t
             :with-footnotes t
             :with-sub-superscript nil
             :with-timestamps nil
             :html-head ""
             :html-head-extra "<link rel='icon' type='image/x-icon' href='/favicon.ico'/>"
             :html-head-extra "<link rel='stylesheet' href='/org.min.css' type='text/css'>"
             ;; :html-home/up-format ""
             :html-head-include-default-style nil
             :html-head-include-scripts nil
             :html-scripts ""
             :html-style-default ""
             :html-use-infojs nil
             :html-validation-link t
             :html-preamble
"<div class='intro' style='padding-top:20px;text-align:center'>
<a href='/'><img src='/lpasrsi.png' alt='LPASRSI'></a>
<h1>Modules « Système Linux »</h1>
<p style='color: #bbb'>Vacations en licence professionnelle « Audit et Sécurité des Réseaux et Systèmes d'Information » à l'antenne de Ifs du pôle de Caen de l'IUT Grand Ouest Normandie</p>
<hr>
</div"
             :html-postamble
"<div class='footer' style='color: #bbb; font-size: xx-small'>
<hr>
<ul>
  <li><a href='https://git.unicaen.fr/lemonniero/lemonniero.pages.unicaen.fr/-/tree/master'>Source du site</a> (<em>fork me!</em>)</li>
  <li>Ces documents sont mis à disposition selon les termes de la licence <a rel='license' href='https://creativecommons.org/licenses/by-nc-sa/4.0'>CC BY-NC-SA 4.0 <img style='height:22px!important;margin-left:3px;vertical-align:text-bottom;' src='https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1' /><img style='height:22px!important;margin-left:3px;vertical-align:text-bottom;' src='https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1' /><img style='height:22px!important;margin-left:3px;vertical-align:text-bottom;' src='https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1' /><img style='height:22px!important;margin-left:3px;vertical-align:text-bottom;' src='https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1' /></a></li>
  <li>Dernière mise à jour : %C</li>
  <li>Fabriqué avec %c !</li>
</ul>
</div>"
             :auto-sitemap nil
             ;; :auto-sitemap t
             ;; :sitemap-style 'list
             ;; :sitemap-title "LPASRSI, modules « Système Linux »"
             ;; :sitemap-sort-files 'alphabetically
             ;; :sitemap-filename "index.org"
             ;; :sitemap-file-entry-format "%d *%t*"
             :makeindex t)
       (list "site-static"
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org"))))

(provide 'publish)
;;; publish.el ends here
